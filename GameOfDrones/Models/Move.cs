﻿namespace GameOfDrones.Models
{
    public class Move
    {
        public string Name { get; set; }
        public string Victim { get; set; }
    }
}