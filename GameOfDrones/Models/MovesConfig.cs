﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameOfDrones.Models
{
    public static class MovesConfig
    {
        private static List<Move> _moves;

        public static List<Move> Moves
        {
            get
            {
                if (_moves == null)
                {
                    _moves = new List<Move>();
                    _moves.Add(new Move
                    {
                        Name = "Rock",
                        Victim = "Scissors"
                    });
                    _moves.Add(new Move
                    {
                        Name = "Scissors",
                        Victim = "Paper"
                    });
                    _moves.Add(new Move
                    {
                        Name = "Paper",
                        Victim = "Rock"
                    });
                }
                return _moves;
            }
        }
    }
}