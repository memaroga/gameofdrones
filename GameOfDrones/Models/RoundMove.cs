﻿namespace GameOfDrones.Models
{
    public class RoundMove
    {
        public string Move { get; set; }
        public string Player { get; set; }
    }
}