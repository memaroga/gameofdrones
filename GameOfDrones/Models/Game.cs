﻿using System.Collections.Generic;

namespace GameOfDrones.Models
{
    public class Game
    {
        public string[] Players { get; set; }
        public string GameWinner { get; set; }
        public IList<Round> Rounds { get; set; }
        public Round CurrentRound { get; set; }
        public Game()
        {
            Rounds = new List<Round>();
            Players = new string[2];
        }
    }
}