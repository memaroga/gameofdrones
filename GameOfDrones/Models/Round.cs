﻿namespace GameOfDrones.Models
{
    public class Round
    {
        public int Id { get; set; }
        public string Winner { get; set; }
        public RoundMove[] Moves { get; set; }
        public Round()
        {
            Moves = new RoundMove[2];
        }
    }
}