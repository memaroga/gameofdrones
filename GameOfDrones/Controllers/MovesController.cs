﻿using GameOfDrones.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GameOfDrones.Controllers
{
    public class MovesController : ApiController
    {
        // GET api/<controller>
        public IHttpActionResult Get()
        {
            return Ok(MovesConfig.Moves);
        }
    }
}