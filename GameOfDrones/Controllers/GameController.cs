﻿using GameOfDrones.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;

namespace GameOfDrones.Controllers
{
    public class GameController : ApiController
    {
        private static Game Game { get; set; }
        private static int CurrentGame { get; set; }
        private const string DRAW = "Draw";
        // GET api/<controller>
        public IHttpActionResult Get()
        {
            return Ok(Game);
        }

        [HttpPost]
        // POST api/<controller>
        public IHttpActionResult CreateGame([FromBody]Game game)
        {
            CurrentGame = CurrentGame++;
            game.CurrentRound = new Round
            {
                Id = 1
            };
            Game = game;
            return Ok(Game);
        }

        [HttpPut]
        public IHttpActionResult PlayRound([FromBody]Round round)
        {
            string winner = GetRoundWinner(round);
            Game.CurrentRound.Winner = string.IsNullOrWhiteSpace(winner) ? DRAW : winner;
            int currentRound = Game.CurrentRound.Id;
            Game.Rounds.Add(Game.CurrentRound);
            Game.GameWinner = GetGameWinner(Game.Rounds);
            if (string.IsNullOrWhiteSpace(Game.GameWinner))
                Game.CurrentRound = new Round
                {
                    Id = currentRound + 1,
                    Moves = new RoundMove[2]
                };
            else
                Game.CurrentRound = null;
            return Ok(Game);
        }

        private string GetRoundWinner(Round round)
        {
            string winnerMove = "";
            string winnerPlayer = "";
            foreach (var move in round.Moves)
            {
                var first = round.Moves.First();
                if(!round.Moves.All(x => x.Move == first.Move) && (string.IsNullOrWhiteSpace(winnerMove) || MovesConfig.Moves.Any(x => x.Name == move.Move && x.Victim == winnerMove)))
                {
                    winnerPlayer = move.Player;
                    winnerMove = move.Move;
                }
            }
            return winnerPlayer;
        }

        private string GetGameWinner(IList<Round> rounds)
        {
            var playersCount = rounds.Where(x => x.Winner != DRAW).GroupBy(x => x.Winner).OrderByDescending(x => x.Count()).Select(g => new { Player = g.Key, Count = g.Count() });
            var winner = playersCount.Where(x => x.Count >= 3).Select(x => x.Player).FirstOrDefault();
            return winner;
        }
    }
}