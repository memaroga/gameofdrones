#Game of Drones: Rock, Paper, Scissors 
This is a solution for playing Rock, Paper, Scissors developed with an Angular Front End and ASP.NET Web Api Back End. 

##Online Demo
You can test it by entering to the [online demo](https://gameofdrones.azurewebsites.net). You can also make test requests by querying the [API](https://gameofdrones-api.azurewebsites.net).

##Run locally
If you want to run the project locally you can follow next steps: 

1. Go to `/GameOfDrones.SPA`
2. Install nvm by running `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash` in an unix based OS terminal. In other cases, please refer to [NVM Documentation](https://github.com/creationix/nvm#installation)
3. Install specified node version with:

    ```
    nvm install
    nvm use
    ```

4. Install npm dependencies: bower, gulp and typings.

    ```
    npm install -g bower gulp typings
    ```

5. Install project dependencies.

    ```
    npm install
    bower install
    typings install
    ```
    
6. Run the project with `gulp serve:dist`