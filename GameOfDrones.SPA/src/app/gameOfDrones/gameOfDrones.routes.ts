/** @ngInject */
export default function routes($stateProvider: angular.ui.IStateProvider,
  $urlRouterProvider: angular.ui.IUrlRouterProvider) {
  $stateProvider
    .state('layout.start', {
      url: 'start',
      template: '<game-start></game-start>'
    })
    .state('layout.roundMove', {
      url: 'play',
      template: '<round-move></round-move>'
    })
    .state('layout.gameEnd', {
      url: 'end',
      template: '<game-end></game-end>'
    });
}
