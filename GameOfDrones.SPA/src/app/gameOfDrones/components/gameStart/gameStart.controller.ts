import { Game } from 'gameOfDrones/dto/Game';
import { GameOfDronesService } from 'gameOfDrones/services/gameOfDrones.service';

export default class GameStartController implements ng.IComponentController {
  public game: Game;

 /** @ngInject */
  constructor(private GameOfDronesService: GameOfDronesService, private $state: angular.ui.IStateService, private toastr: any) {
    this.game = new Game();
  }

  submit(): void {
    this.GameOfDronesService.startGame(this.game).then(
        (game: Game) => this.$state.go('layout.roundMove'),
        (error: any) => this.toastr.error('An error has occurred: ' + error.data)
    );

  }
}
