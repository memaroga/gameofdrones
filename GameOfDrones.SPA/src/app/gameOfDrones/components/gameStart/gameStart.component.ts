import GameStartController from './gameStart.controller';

export const GameStartComponent: angular.IComponentOptions = {
  templateUrl: 'app/gameOfDrones/components/gameStart/gameStart.html',
  controller: GameStartController
};
