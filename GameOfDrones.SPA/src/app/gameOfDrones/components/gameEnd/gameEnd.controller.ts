import { GameOfDronesService } from 'gameOfDrones/services/gameOfDrones.service';

export default class GameEndController implements ng.IComponentController {
  public gameWinner: string;

 /** @ngInject */
  constructor(private GameOfDronesService: GameOfDronesService, private $state: angular.ui.IStateService, private toastr: any) {
    var that = this;
    that.GameOfDronesService.getGameWinner().then(
        (gameWinner: string) => that.gameWinner = gameWinner,
        (error: any) => this.toastr.error('An error has occurred: ' + error.data)
    );
  }
}
