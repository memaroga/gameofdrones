import GameEndController from './gameEnd.controller';

export const GameEndComponent: angular.IComponentOptions = {
  templateUrl: 'app/gameOfDrones/components/gameEnd/gameEnd.html',
  controller: GameEndController
};
