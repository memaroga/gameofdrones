import { Game } from 'gameOfDrones/dto/Game';
import { Move } from 'gameOfDrones/dto/Move';
import { RoundMove } from 'gameOfDrones/dto/RoundMove';
import { GameOfDronesService } from 'gameOfDrones/services/gameOfDrones.service';

export default class RoundMoveController implements ng.IComponentController {
  public game: Game;
  public moves: Move[];
  public currentPlayerIndex: number;
  /** @ngInject */
  constructor(private GameOfDronesService: GameOfDronesService, private $state: angular.ui.IStateService, private toastr: any) {
    var that = this;
    that.currentPlayerIndex = 0;
    GameOfDronesService.getGame().then(
      (game: Game) => {
        that.game = game;
        GameOfDronesService.getMoves().then(
          (moves: Move[]) => {
            that.moves = moves;
          },
          (error: any) => that.toastr.error('An error has occurred: ' + error.data)
        );
      },
      (error: any) => that.toastr.error('An error has occurred: ' + error.data)
    );
  }

  saveRound(): void {
    var that = this;
    if (this.currentPlayerIndex === this.game.Players.length - 1) {
      this.game.CurrentRound.Moves.forEach((move: RoundMove, i: number) => {
        move.Player = that.game.Players[i];
      });
      this.GameOfDronesService.playRound(this.game.CurrentRound).then(
        (game: Game) => {
          if (game.GameWinner) {
            that.$state.go('layout.gameEnd');
          } else {
            that.game = game;
            that.currentPlayerIndex = 0;
          }
        },
        (error: any) => that.toastr.error('An error has occurred: ' + error.data)
      );
    } else {
      this.currentPlayerIndex = this.currentPlayerIndex + 1;
    }
  }
}
