import RoundMoveController from './roundMove.controller';

export const RoundMoveComponent: angular.IComponentOptions = {
  templateUrl: 'app/gameOfDrones/components/roundMove/roundMove.html',
  controller: RoundMoveController
};
