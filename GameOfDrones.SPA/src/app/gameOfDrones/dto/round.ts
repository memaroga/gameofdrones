import { RoundMove } from './RoundMove';

export class Round {
    Id: number;
    Winner: string;
    Moves: RoundMove[];
    constructor() {
        let _roundMoves = [];
        _roundMoves.push(new RoundMove());
        _roundMoves.push(new RoundMove());
        this.Moves = _roundMoves;
    }
}
