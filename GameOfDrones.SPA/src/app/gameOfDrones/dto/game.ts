import { Round } from './Round';

export class Game {
  Players: string[];
  Rounds?: Round[];
  CurrentRound?: Round;
  GameWinner: string;

  constructor() {
    let _players = [];
    _players.push('');
    _players.push('');
    this.Players = _players;
  }
}
