import { Game } from 'gameOfDrones/dto/Game';
import { Move } from 'gameOfDrones/dto/Move';
import { Round } from 'gameOfDrones/dto/Round';
import { Environment } from 'common/environment/environment';

export class GameOfDronesService {

    private game: Game;
    private url: string;
    private moves: Move[];
    private gameWinner: string;

    /** @ngInject */
    constructor(private $q: angular.IQService, private $timeout: angular.ITimeoutService, private $http: angular.IHttpService, private environment: Environment) {
        this.url = environment.api;
    }

    public playRound(_round: Round): angular.IPromise<Game> {
        var roundJson = JSON.stringify(_round);
        var that = this;
        var deferred = this.$q.defer<Game>();
        this.$http({
            url: that.url + '/Game/PlayRound',
            method: 'PUT',
            data: roundJson
        }).then(function (response: any) {
            that.game = <Game>response.data;
            deferred.resolve(that.game);
        });
        return deferred.promise;
    }

    public startGame(_game: Game): angular.IPromise<Game> {
        var deferred = this.$q.defer<Game>();
        var gameJson = JSON.stringify(_game);
        var that = this;
        this.$http({
            url: that.url + '/Game/CreateGame',
            method: 'POST',
            data: gameJson
        }).then(function (response: any) {
            that.game = <Game>response.data;
            deferred.resolve(that.game);
        });
        return deferred.promise;
    }

    public getGame(): angular.IPromise<Game> {
        var deferred = this.$q.defer<Game>();
        var that = this;
        that.$http({
            url: that.url + '/Game',
            method: 'GET'
        }).then(function (response: any) {
            that.game = <Game>response.data;
            deferred.resolve(that.game);
        });
        return deferred.promise;
    }

    public getGameWinner(): angular.IPromise<string> {
        var deferred = this.$q.defer<string>();
        var that = this;
        if (this.gameWinner) {
            deferred.resolve(this.gameWinner);
        } else {
            this.getGame().then(function (game: Game) {
                that.gameWinner = game.GameWinner;
                deferred.resolve(<string>game.GameWinner);
            });
        }
        return deferred.promise;
    }

    public getMoves(): angular.IPromise<Move[]> {
        var that = this;
        var deferred = this.$q.defer<Move[]>();
        if (!that.moves) {
            that.$http({
                url: that.url + '/Moves',
                method: 'GET'
            }).then(function (response: any) {
                that.moves = <Move[]>response.data;
                deferred.resolve(that.moves);
            });
        } else {
            deferred.resolve(that.moves);
        }
        return deferred.promise;
    }

}
