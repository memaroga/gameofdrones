import common from 'common/common.module';
import routes from './gameOfDrones.routes';
import { GameStartComponent } from 'gameOfDrones/components/gameStart/gameStart.component';
import { RoundMoveComponent } from 'gameOfDrones/components/roundMove/roundMove.component';
import { GameEndComponent } from 'gameOfDrones/components/gameEnd/gameEnd.component';
import { GameOfDronesService } from 'gameOfDrones/services/gameOfDrones.service';

export default angular.module('app.gameOFDrones', [common.name])
  .config(routes)
  .service('GameOfDronesService', GameOfDronesService)
  .component('gameStart', GameStartComponent)
  .component('roundMove', RoundMoveComponent)
  .component('gameEnd', GameEndComponent);
