/// <reference path="../../typings/index.d.ts" />

import { environment } from 'environment';
import common from 'common/common.module';
import gameOfDrones from 'gameOfDrones/gameOfDrones.module';

import routes from './app.routes';
import forms from './app.forms';
import run from './app.run';

angular.module('app', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngMessages',
    'ngSanitize', 'ngAria', 'angularPromiseButtons',
    'toastr', 'ui.router', 'ngFabForm',
    common.name, gameOfDrones.name])
  .constant('environment', environment)
  .config(routes)
  .config(forms)
  .run(run);
