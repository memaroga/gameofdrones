import { RootComponent } from './components/root/root.component';
import { HeaderComponent } from './components/header/header.component';

export default angular.module('app.common', [])
  .component('appRoot', RootComponent)
  .component('header', HeaderComponent);
