import {Environment} from './environment';

export const environment: Environment = {
  name: 'development',
  api: 'https://gameofdrones-api.azurewebsites.net/api',
  port: '8081',
  path: '',
  timeout: 30000
};
