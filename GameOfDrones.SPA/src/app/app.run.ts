import {Environment} from 'common/environment/environment';

/** @ngInject */
export default function run($log: angular.ILogService, environment: Environment) {
  $log.debug(`Application in ${environment.name} mode`);
}
