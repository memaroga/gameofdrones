# Deployment  
# ----------  

echo Handling node.js gulp deployment.  

SPA_SOURCE=$DEPLOYMENT_SOURCE/GameOfDrones.SPA

npm install -g gulp bower typings

# 2. Install npm packages  
if [ -e "$SPA_SOURCE/package.json" ]; then
  cd "$SPA_SOURCE"  
  npm install  
fi  

# 3. Install bower packages  
if [ -e "$SPA_SOURCE/bower.json" ]; then
  cd "$SPA_SOURCE"  
  bower install  
fi  

# 4. Install typings
if [ -e "$SPA_SOURCE/typings.json" ]; then
  cd "$SPA_SOURCE"  
  typings install  
fi  

# 5. Run gulp for build
if [ -e "$SPA_SOURCE/gulpfile.js" ]; then
  cd "$SPA_SOURCE"  
  gulp build  
fi  

# 6. KuduSync to Target  
"$KUDU_SYNC_CMD" -v 500 -f "$SPA_SOURCE/dist" -t "$DEPLOYMENT_TARGET" -n "$NEXT_MANIFEST_PATH" -p "$PREVIOUS_MANIFEST_PATH" -i ".git;.hg;.deployment;deploy.sh"  
exitWithMessageOnError "Kudu Sync to Target failed"